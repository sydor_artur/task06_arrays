package com.epam.model.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Mini Game.
 */
public class GameWithHero {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    private int heroStrength;
    private int[] closedDoors;
    private int numberOfDeathDoors;
    private int counter = 0;

    /**
     * Constructor.
     */
    public GameWithHero() {
        heroStrength = 25;
        closedDoors = initializeDoors();
    }

    /**
     * Show all doors and their values.
     */
    public void showDoors() {
        for (int door : closedDoors) {
            if (door > 0) {
                logger.info("Artifact - " + door);
            } else {
                logger.info("Monster - " + door * (-1));
            }
        }
    }

    /**
     * Start a game.
     */
    public void play() {
        int iteration = 0;
        while (isClosed(closedDoors)) {
            iteration++;
            int heroChoice = (int) (Math.random() * 9);
            if (closedDoors[heroChoice] > 0) {
                heroStrength += closedDoors[heroChoice];
                closedDoors[heroChoice] = 0;
                logger.info("Hero find artifact now his strength = " + heroStrength);
            } else {
                heroStrength += closedDoors[heroChoice];
                closedDoors[heroChoice] = 0;
                if (heroStrength < 1) {
                    logger.info("Monster defeat a hero");
                    return;
                } else {
                    if (iteration > 1000) {
                        break;
                    }
                    continue;
                }
            }
        }
        logger.info("Hero won. Now his strength is " + heroStrength);
    }

    /**
     * Count number of door where hero can die.
     * Recursive is used here.
     */
    public void countDeath() {
        if (closedDoors[counter] < 0) {
            int tempStrength = heroStrength + closedDoors[counter];
            if (tempStrength < 1) {
                numberOfDeathDoors++;
            }
        }
        counter++;
        if (counter == closedDoors.length) {
            logger.info("Number of death door equals " + numberOfDeathDoors);
            return;
        }
        countDeath();
    }

    /**
     * Show the best variant of end.
     */
    public void showBestVariant() {
        int artifact = 25;
        int damage = 0;
        for (int i = 0; i < closedDoors.length; i++) {
            if (closedDoors[i] > 0) {
                artifact += closedDoors[i];
                logger.info("Door #" + (i + 1) + "|Artifact|");
            }
        }
        for (int i = 0; i < closedDoors.length; i++) {
            if (closedDoors[i] < 0) {
                artifact += closedDoors[i];
                logger.info("Door #" + (i + 1) + "|Monster|");
            }
        }
        if (artifact >= damage) {
            logger.info("It`s way to victory");
        } else {
            logger.info("Monsters are too strong");
        }
    }

    /**
     * @param arr with doors.
     * @return true if there left closed doors.
     */
    private boolean isClosed(int[] arr) {
        for (int item : arr) {
            if (item != 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return initialized doors.
     */
    private int[] initializeDoors() {
        int[] doors = new int[10];
        for (int i = 0; i < 10; i++) {
            int artifact = (int) (Math.random() * 71) + 10;
            if (artifact % 2 == 0) {
                doors[i] = artifact;
            } else {
                doors[i] = -1 * (int) (Math.random() * 96) + 5;
            }
        }
        return doors;
    }

}
