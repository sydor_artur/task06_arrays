package com.epam.model.arrays;

import java.util.Comparator;

public class CompareByCapital implements Comparator<CountryCapital> {
    @Override
    public int compare(CountryCapital countryCapital, CountryCapital t1) {
        return countryCapital.getCapital().compareTo(t1.getCapital());
    }
}
