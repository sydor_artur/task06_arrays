package com.epam.model.arrays;

import com.epam.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CountryCapital implements Comparable<CountryCapital>, Model {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    private String country;
    private String capital;

    public CountryCapital() {

    }

    public CountryCapital(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(CountryCapital countryCapital) {
        return this.country.compareTo(countryCapital.country);
    }

    public String giveInfo() {
        String[] countries = {"Ukraine", "France", "England", "Germany", "Spain"};
        String[] capitals = {"Kiev", "Paris", "London", "Berlin", "Madrid"};
        StringBuilder str = new StringBuilder();

        Random rand = new Random();
        List<CountryCapital> list = new ArrayList<>();
        for (int i = 0; i < countries.length; i++) {
            int number = rand.nextInt(5);
            list.add(new CountryCapital(countries[number], capitals[number]));
        }
        str.append("Initialized list\n");
        list.forEach(pair -> {
            str.append(pair.country + " " + pair.capital + "\n");
        });

        Collections.sort(list);

        str.append("After sorting with Comparable\n");
        list.forEach(pair -> {
            str.append(pair.country + " " + pair.capital + "\n");
        });
        str.append("After sorting with Comparator\n");
        Collections.sort(list, new CompareByCapital());
        list.forEach(pair -> {
            str.append(pair.country + " " + pair.capital + "\n");
        });

        str.append("Binary search\n");
        str.append(Collections.binarySearch(list,
                new CountryCapital(null, "Madrid"),
                new CompareByCapital()));
        return str.toString();
    }
}
