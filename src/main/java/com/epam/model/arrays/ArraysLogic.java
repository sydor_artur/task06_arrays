package com.epam.model.arrays;

import com.epam.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ArraysLogic implements Model {
    public static int[] joinArrays(int[] arr1, int[] arr2) {
        int[] result = null;
        List<Integer> utility = new ArrayList<>();
        int elemCounter = 0;
        for(int i = 0; i < arr1.length; i++) {
            for(int j = 0; j < arr2.length; j++) {
                if(arr1[i] == arr2[j]) {
                    utility.add(arr1[i]);
                }
            }
        }
        result = new int[utility.size()];
        for (Integer elem : utility) {
            result[elemCounter++] = elem;
        }

        return result;
    }

    public static int[] elemFromOneArray(int[] arr1, int[] arr2) {
        int[] result = null;
        Stack<Integer> utility = new Stack<>();
        int elemCounter = 0;
        for(int i = 0; i < arr1.length; i++) {
            utility.push(arr1[i]);
            for(int j = 0; j < arr2.length; j++) {
                if(arr1[i] == arr2[j]) {
                    utility.pop();
                    break;
                }
            }
        }
        result = new int[utility.size()];
        for (Integer elem : utility) {
            result[elemCounter++] = elem;
        }

        return result;
    }

    public static int[] deleteRepeated(int[] arr) {
        int[] result = null;
        List<Integer> utility = new ArrayList<>();
        int killerOfRepeatedElem;

        for (int i = 0; i < arr.length; i++) {
            killerOfRepeatedElem = 1;
            for (int j = 0; j < arr.length; j++) {
                if(i == j) {
                    continue;
                }
                if(arr[i] == arr[j]) {
                    killerOfRepeatedElem++;
                }
            }
            if(killerOfRepeatedElem <= 2) {
                utility.add(arr[i]);
            }
        }

        int elemCounter = 0;
        result = new int[utility.size()];
        for (Integer elem : utility) {
            result[elemCounter++] = elem;
        }

        return result;
    }

    public static int[] findSeries(int[] arr) {
        int[] result = null;
        List<Integer> utility = new ArrayList<>();
        for(int i = 0; i < arr.length;) {
            if(i == arr.length - 1) {
                break;
            }
            utility.add(arr[i]);
            int counter = i + 2;
            for(int j = i + 1; j < counter; j++) {
                if(arr[i] != arr[j] || j == arr.length - 1) {
                    i = j;
                    break;
                } else {
                    counter++;
                }
            }
        }

        int elemCounter = 0;
        result = new int[utility.size()];
        for (Integer elem : utility) {
            result[elemCounter++] = elem;
        }

        return result;
    }

    @Override
    public String giveInfo() {
        StringBuilder str = new StringBuilder();
        str.append("Join arrays\n");
        for(Integer i : joinArrays(new int[]{3, 5, 6,7 ,7,9}, new int[]{6, 34, 54,3, 9,100})) {
            str.append(i + " ");
        }
        str.append("\nElem from one array\n");
        for(Integer i : elemFromOneArray(new int[]{3, 5, 6, 71, 7, 9}, new int[]{6, 34, 54,3, 9,100})) {
            str.append(i + " ");
        }
        str.append("\nDelete repeated arrays\n");
        for(Integer i : deleteRepeated(new int[]{3, 5, 5, 6, 7, 3, 3, 3, 71, 7, 9, 7})) {
            str.append(i + " ");
        }
        str.append("\nFind and delete series\n");
        for(Integer i : findSeries(new int[]{1, 1, 1, 3, 5, 5, 6, 7, 3, 3, 3, 71, 7, 9, 7, 7, 7 ,7, 1, 1,1})) {
            str.append(i + " ");
        }
        return str.toString();
    }
}
