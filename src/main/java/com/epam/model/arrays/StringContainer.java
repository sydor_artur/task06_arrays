package com.epam.model.arrays;

/**
 * @param <T> contains only strings.
 */
public class StringContainer<T extends String> {
    private String[] listOfStrings;
    private int size;
    private int position;

    /**
     * Default constructor.
     */
    public StringContainer() {
        position = 0;
        size = 10;
        listOfStrings = new String[size];
    }

    /**
     * @param size from user.
     */
    public StringContainer(int size) {
        position = 0;
        this.size = size;
        listOfStrings = new String[size];
    }

    /**
     * @param item which user want to add to container.
     */
    public void add(String item) {
        if (size - 1 > position) {
            listOfStrings[position++] = item;
        } else {
            listOfStrings = resize(listOfStrings);
            listOfStrings[position++] = item;
        }
    }

    /**
     * @param index index of item.
     * @return item.
     */
    public String getItem(int index) {
        if (index >= position) {
            return "";
        } else {
            return listOfStrings[index];
        }
    }

    /**
     * @return size of container.
     */
    public int size() {
        return position;
    }

    /**
     * @param list old container.
     * @return container with new size.
     */
    private String[] resize(String[] list) {
        int newSize = list.length / 2 + list.length;
        String[] newList = new String[newSize];

        for (int i = 0; i < list.length; i++) {
            newList[i] = list[i];
        }

        return newList;
    }

}
