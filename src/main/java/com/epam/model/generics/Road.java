package com.epam.model.generics;

import java.util.ArrayList;
import java.util.List;

public class Road<T extends Vehicle> {
    private T unit;
    private List<? super T> vehicles;

    public Road() {
        vehicles = new ArrayList<>();
    }

    public Road(List<? super T> vehicles) {
        this.vehicles = vehicles;
    }

    public List<? super T> getVehicles() {
        return vehicles;
    }

    public T getUnit(int index) {
        return (T)vehicles.get(index);
    }

    public T getUnit() {
        return unit;
    }

    public void setUnit(T unit) {
        this.unit = unit;
        vehicles.add(unit);
    }

}
