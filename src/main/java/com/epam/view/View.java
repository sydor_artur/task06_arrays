package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {
    private static Logger logger = LogManager.getLogger("InfoForUser");

    public void print(String string) {
        logger.info(string);
    }
}
