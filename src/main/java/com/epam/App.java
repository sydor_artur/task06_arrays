package com.epam;

import com.epam.controller.Controller;
import com.epam.dataStructures.MyDeque;
import com.epam.model.Model;
import com.epam.model.arrays.ArraysLogic;
import com.epam.model.arrays.CountryCapital;
import com.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class App {
    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        View view = new View();
        Model model = new ArraysLogic();
        Controller controller = new Controller(model, view);
        controller.showArrays();
        logger.info("----------------------");
        model = new CountryCapital();
        Controller controller1 = new Controller(model, view);
        controller1.showTestOfOwnContainer();
        logger.info("----------------------");
        controller.playGame();
        logger.info("----------------------");

        //Test MyDeque
        MyDeque<Integer> q = new MyDeque<>();
        q.insertFront(333);
        q.insertFront(4);
        q.insertRear(3);
        q.insertFront(5);
        logger.info(q.getFront());
        q.deleteFront();
        logger.info(q.getFront());
        logger.info(q.getRear());
        q.deleteRear();
        logger.info(q.getRear());
        q.deleteRear();
        q.deleteFront();
        logger.info(q.getRear());

    }
}
