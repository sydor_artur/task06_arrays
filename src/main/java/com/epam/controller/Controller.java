package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.arrays.GameWithHero;
import com.epam.view.View;

public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    private String takeArraysInfo() {
        return model.giveInfo();
    }

    private String takeTestOfOwnContainer() {
        return model.giveInfo();
    }

    public void showArrays() {
        view.print(takeArraysInfo());
    }

    public void showTestOfOwnContainer() {
        view.print(takeTestOfOwnContainer());
    }

    public void playGame() {
        GameWithHero game = new GameWithHero();
        game.showDoors();
        game.countDeath();
        game.showBestVariant();
        game.play();
    }
}
