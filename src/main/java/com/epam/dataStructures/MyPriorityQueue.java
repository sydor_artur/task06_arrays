package com.epam.dataStructures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * My own priority queue.
 *
 * @param <T> generic type.
 */
public class MyPriorityQueue<T> {
    private Comparable[] priorQueue;
    private int numberOfItems;
    static Logger logger = LogManager.getLogger("InfoForUser");


    /**
     * Constructor.
     *
     * @param capacity of queue.
     */
    public MyPriorityQueue(int capacity) {
        priorQueue = new Comparable[capacity];
    }

    /**
     * This function add item to queue.
     */
    public void insert(Comparable item) {
        if (numberOfItems == priorQueue.length) {
            logger.info("The priority queue is full!");
            return;
        }
        priorQueue[numberOfItems] = item;
        numberOfItems++;
    }

    /**
     * Remove item with the biggest priority.
     *
     * @return deleted item.
     */
    public Comparable remove() {
        if (isEmpty()) {
            logger.info("The priority queue is empty!");
            return null;
        }

        int maxItemIndex = 0;
        for (int i = 1; i < numberOfItems; i++) {
            if (priorQueue[i].compareTo(priorQueue[maxItemIndex]) > 0) {
                maxItemIndex = i;
            }
        }
        Comparable result = priorQueue[maxItemIndex];
        numberOfItems--;
        priorQueue[maxItemIndex] = priorQueue[numberOfItems];
        return result;
    }

    /**
     * @return true if queue is empty.
     */
    public boolean isEmpty() {
        return numberOfItems == 0;
    }

    /**
     * @return size of queue.
     */
    public int size() {
        return numberOfItems;
    }

    /**
     * @param index of item.
     * @return value by index.
     */
    public Comparable getValue(int index) {
        return priorQueue[index];
    }
}