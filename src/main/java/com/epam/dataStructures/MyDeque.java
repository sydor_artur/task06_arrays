package com.epam.dataStructures;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * My own implementation of deque.
 *
 * @param <T> of item in dequeue.
 */
public class MyDeque<T> {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    private Object[] arrayToSaveItems;
    private int front;
    private int rear;
    private int size;
    public static final int MAX_CAPACITY = 20;

    /**
     * Constructor.
     */
    public MyDeque() {
        arrayToSaveItems = new Object[MAX_CAPACITY];
        front = -1;
        rear = 0;
    }

    /**
     * @return true if is full.
     */
    public boolean isFull() {
        return size == MAX_CAPACITY - 1;
    }

    /**
     * @return true if is empty.
     */
    public boolean isEmpty() {
        return size < 1;
    }


    /**
     * @param item to insert in front.
     */
    public void insertFront(T item) {
        if (isFull()) {
            return;
        }

        if (front == -1) {
            front = 0;
            rear = 0;
        } else if (front == 0) {
            front = MAX_CAPACITY - 1;
        } else {
            front--;
        }
        arrayToSaveItems[front] = item;
        size++;
    }

    /**
     * @param item to insert in rear.
     */
    public void insertRear(T item) {
        if (isFull()) {
            return;
        }

        if (front == -1) {
            front = 0;
            rear = 0;
        } else if (rear == MAX_CAPACITY - 1) {
            rear = 0;
        } else {
            rear++;
        }
        arrayToSaveItems[rear] = item;
        size++;
    }

    /**
     * Delete item from front.
     */
    public void deleteFront() {
        if (isEmpty()) {
            return;
        }

        if (front == rear) {
            front = -1;
            rear = -1;
        } else if (front == MAX_CAPACITY - 1) {
            front = 0;
        } else {
            front++;
        }
        size--;
    }

    /**
     * Delete item from rear.
     */
    public void deleteRear() {
        if (isEmpty()) {
            return;
        }

        if (front == rear) {
            front = -1;
            rear = -1;
        } else if (rear == 0) {
            rear = MAX_CAPACITY - 1;
        } else {
            rear--;
        }
        size--;
    }

    /**
     * @return item form front.
     */
    public T getFront() {
        if (isEmpty()) {
            return null;
        } else {
            return (T) arrayToSaveItems[front];
        }
    }

    /**
     * @return item from rear.
     */
    public T getRear() {
        if (isEmpty()) {
            return null;
        } else {
            return (T) arrayToSaveItems[rear];
        }
    }

}
